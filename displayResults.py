import matplotlib.pyplot as plt
import random

def display(instance, result):
    display_grid(instance)
    display_visited_customers(instance, result)
    display_used_arc(instance, result)
    plt.ylim(-30,30) #limites axe abscisses
    plt.xlim(-30,30) #limites axe ordonnées
    plt.scatter(instance["customers"][0]["x"],instance["customers"][0]["y"],marker='*',color='orange',s=200) #tracé du point de dépôt
    plt.title("Profit total : %d"%(result["total_profit"]))
    plt.show() #affiche le graphique final


def display_grid(instance): #fonction qui affiche les clients de l'instance
    for i in range (len(instance["customers"])): #parcours du tableau des clients de l'instance pour récupérer leurs coordonnées
        x=instance["customers"][i]["x"]
        y=instance["customers"][i]["y"]
        plt.scatter(x,y,marker='p',color='black',s=20) #tracé des points correspondants à l'emplacement de chaque client
        #plt.annotate(customers[i][2],xy=(x+0.5,y+0.5)) #affiche les profits associés à chaque point


def display_visited_customers(instance,result):#fonction qui affiche les clients visités
    print(instance)
    for i in range (len(result["customer_visited"])): #parcours du tableau des clients visités pour récupérer leurs coordonnées
        for k in range (instance["vehicles_count"]):
            if(result["customer_visited"][i][k])== 1:
                x= instance["customers"][i]["x"]
                y= instance["customers"][i]["y"]
                ##x=customers_visited[i][0]
                ##y=customers_visited[i][1]
                plt.scatter(x,y,marker='p',color='red',s=20) #tracé des points correspondants à l'emplacement de chaque client visité


def display_used_arc(instance,result): #fonction qui affiche les arcs parcourus
    '''partie à revoir en fonction de ce qui est renvoyé dans arc_used par instanceBuild'''
    for k in range(instance["vehicles_count"]):
        r = random.random()
        b = random.random()
        g = random.random()
        color = (r,g,b)
        for i in range (len(result["arc_used"])):
            for j in range (len(result["arc_used"][i])):
                if(result["arc_used"][i][j][k])==1:
                    x1= instance["customers"][i]["x"]
                    y1= instance["customers"][i]["y"]
                    x2= instance["customers"][j]["x"]
                    y2= instance["customers"][j]["y"]

                    plt.plot([x1,x2],[y1,y2],c=color) #trace une ligne représentant l'arc parcouru
