import json
import random
import numpy as np

def init():
    instance = {}
    max_distance, vehicles_count, customers_count, customers = 0, 0, 0, []

    ##TODO: console utilisateur, génération manuelle/auto ? choix d'instance ? ...

    print("Bonjour, bienvenue sur le programme de résolution du projet AI09, choisissez un mode de génération des variables : auto (0), manuel(1) ")
    choice = input()
    # tester l'entrée


    if choice =="0":
        instance = random_init()
    else:
        print("Veuillez entrez le nom de votre fichier json ( avec l'extension .json )")
        json_name = input()
        instance = existing_init(json_name)

    return instance


def random_init():
    max_distance, vehicles_count, customers_count, customers = 0, 0, 0, []
    max_distance = random.randint(50,200) ## On considère une distance max de 1000 km par véhicule
    print(max_distance)
    vehicles_count = random.randint(1,20) ## On considère au maximum 200 véhicules
    print(vehicles_count)
    customers_count = (max_distance*vehicles_count)//2 ## On utilise la distance maximale et le nombre de véhicules pour approcher un nombre de clients souhaitable pour la taille du problème
    print(customers_count)
    customers = [{"x":0, "y":0, "profit":0}]*customers_count
    customers[0] = {"x":random.randint(-30,30), "y":random.randint(-30,30), "profit":0}
    for i in range (1,(len(customers))):
        customers[i] = {"x":random.randint(-30,30), "y":random.randint(-30,30), "profit":random.randint(0,500)}

    instance = {"max_distance": max_distance,
                "vehicles_count": vehicles_count,
                "customers_count": customers_count,
                "customers": customers}

    return instance

## json_name nom du fichier avec l'extension .json
def existing_init(json_name):
    max_distance, vehicles_count, customers_count, customers = 0, 0, 0, []

    f = open(json_name)
    data = json.load(f)
    instance = data['instance']
    max_distance= instance[0]['variables']['maxDistance']
    vehicles_count = instance[0]['variables']['vehicleNumber']
    customers_count = instance[0]['variables']['customerCount']
    customers = instance[0]['customers']

    instance = {"max_distance": max_distance,
                "vehicles_count": vehicles_count,
                "customers_count": customers_count,
                "customers": customers}


    f.close()
    return instance


def init_distance_tab(instance):
    distance = [[0 for x in range(instance["customers_count"])] for y in range(instance["customers_count"])]
    for i in range(instance["customers_count"]):
        customer_i = instance["customers"][i]
        for j in range(instance["customers_count"]):
            customer_j = instance["customers"][j]
            distance[i][j] = np.sqrt((customer_i["x"] - customer_j["x"]) ** 2 + (customer_i["y"] - customer_j["y"] )** 2)

    return distance
