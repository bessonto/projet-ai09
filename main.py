import instanceBuild
import instanceResolving
import displayResults

instance = instanceBuild.init()
distance = instanceBuild.init_distance_tab(instance)

result = instanceResolving.resolve(instance, distance)

displayResults.display(instance,result)

"""
instance = {"L": 0, "m": 0, "n": 0, "clients": [{"x":1, "y": 12, "profit": 4}, {"x":11, "y": 2, "profit": 12}]}
L = 0
m = 0
client = {"x": 0, "y": 0, "cost": 0}
distances = instanceBuild.init_distance_tab()
customerVisited = [[]]
arcUsed = [[]]
"""
