def resolve(instance, distance):
    ## customer_visited = [[]]
    interest = init_interest(instance, distance)

    customer_visited = [[0 for y in range(instance["vehicles_count"])]for x in range(len(interest))]
    arc_used = [[[0 for y in range (instance["vehicles_count"])] for x in range (len(interest))]for z in range (len(interest))]

    ## interest = init_interest(instance, distance)

    for k in range(instance["vehicles_count"]):
        remaining_distance = instance["max_distance"]
        current_customer = 0
        while remaining_distance > 0:
            max_interest_customer = current_customer
            """
            Ici, pour chaque véhicule, nous cherchons la liste des clients consécutifs vers lesquels nous pouvons aller en respectant la limite de distance maximale.
            Pour ce faire, on cherche le client d'intérêt maximal par rapport à notre position actuelle.
            Si nous avons assez de distance restante pour aller à ce client puis retourner au dépot derrière, alors on pourrait y aller.
            Il reste enfin à vérifier que ce clients n'ait pas déjà été desservi précédemment, s'il ne l'a pas été alors on y va !
            """
            for i in range(len(interest)):
                if interest[current_customer][i] > interest[current_customer][max_interest_customer]:
                    if distance[current_customer][i] + distance[i][0] <= remaining_distance:
                        visited = 0
                        for vehicles in range(instance["vehicles_count"]):
                            if customer_visited[i][vehicles] == 1:
                                visited = 1
                        if visited == 0:
                            max_interest_customer = i

            arc_used[current_customer][max_interest_customer][k] = 1

            if max_interest_customer != 0:
                customer_visited[max_interest_customer][k] = 1
                remaining_distance -= distance[current_customer][max_interest_customer]
            else:
                remaining_distance = 0

            current_customer = max_interest_customer

    total_profit = calculate_profit(instance, customer_visited)

    result = {"customer_visited": customer_visited,
              "arc_used": arc_used,
              "total_profit": total_profit}

    return result


def init_interest(instance, distance):
    ## interest = [[]]
    interest = [[0 for x in range(instance["customers_count"])] for y in range(instance["customers_count"])]

    for i in range(instance["customers_count"]):
        for j in range(instance["customers_count"]):
            if i != j:
                interest[i][j] = instance["customers"][j]["profit"] / distance[i][j]
            else:
                interest[i][j] = -1

    return interest


def calculate_profit(instance, customer_visited):
    total_profit = 0
    for i in range (len(customer_visited)):
        for j in range (instance["vehicles_count"]):
            if customer_visited[i][j] == 1 :
                total_profit += instance["customers"][i].get("profit")

    return total_profit
